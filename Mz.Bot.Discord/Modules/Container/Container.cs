﻿using System.Diagnostics.CodeAnalysis;
using Discord.Commands;

using Mz.Bot.Discord.Models.Configuration;

namespace Mz.Bot.Discord
{
    [Group("container")]
    [SuppressMessage("ReSharper", "RedundantExtendsListEntry")]
    public partial class ContainerModule : ModuleBase<SocketCommandContext>
    {
        private readonly Configuration _config;

        public ContainerModule(Configuration config)
        {
            _config = config;
        }
    }
}
