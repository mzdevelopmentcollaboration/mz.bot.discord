﻿using System;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Mz.Package.Portainer.Methods;

// ReSharper disable once CheckNamespace
namespace Mz.Bot.Discord
{
    // ReSharper disable once RedundantExtendsListEntry
    public partial class ContainerModule : ModuleBase<SocketCommandContext>
    {
        [RequireOwner]
        [Command("stop")]
        [Summary("Stops a defined Container.")]
        public Task StopContainerAsync([Remainder] string containerName)
        {
            var loginManager = new Login();
            var embed = new EmbedBuilder
            {
                Title = "Container Status:"
            };
            try
            {
                var portainerWebToken = loginManager.GetLoginToken(
                    new Uri(Environment.GetEnvironmentVariable("portainerAuthenticationEndpoint") ??
                            throw new InvalidOperationException("No portainerAuthenticationEndpoint was found!")),
                    Environment.GetEnvironmentVariable("portainerUsername"),
                    Environment.GetEnvironmentVariable("portainerPassword"));
                var containerManager = new Container();
                var containerList = containerManager.GetContainerCollection(portainerWebToken.jsonWebToken,
                    new Uri(Environment.GetEnvironmentVariable("portainerContainerEndpoint") ??
                            throw new InvalidOperationException("No portainerContainerEndpoint was found!")));
                containerManager.StopContainer(containerList, containerName, portainerWebToken.jsonWebToken,
                    new Uri(Environment.GetEnvironmentVariable("portainerContainerEndpoint") ??
                            throw new InvalidOperationException("No portainerContainerEndpoint was found!")));
                embed.AddField("The Container has been stopped!",
                    "If this does not seem to be the Case, please contact the Admin!");
            }
            catch (Exception)
            {
                embed.AddField("There was an Error while starting the Container!", "Please contact the Admin!");
            }
            ReplyAsync(embed: embed.Build());
            return Task.CompletedTask;
        }
    }
}
