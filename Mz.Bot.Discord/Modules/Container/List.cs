﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Mz.Package.Portainer.Methods;

// ReSharper disable once CheckNamespace
namespace Mz.Bot.Discord
{
    // ReSharper disable once RedundantExtendsListEntry
    public partial class ContainerModule : ModuleBase<SocketCommandContext>
    {
        [Command("list")]
        [Summary("Lists all Containers.")]
        public Task ListContainerAsync()
        {
            var portainerLoginManager = new Login();
            var embed = new EmbedBuilder();
            try
            {
                var portainerWebToken = portainerLoginManager.GetLoginToken(
                    new Uri(Environment.GetEnvironmentVariable("portainerAuthenticationEndpoint") ??
                            throw new InvalidOperationException("No portainerAuthenticationEndpoint was found!")),
                    Environment.GetEnvironmentVariable("portainerUsername"),
                    Environment.GetEnvironmentVariable("portainerPassword"));
                var containerManager = new Container();
                var containerList = containerManager.GetContainerCollection(portainerWebToken.jsonWebToken,
                    new Uri(Environment.GetEnvironmentVariable("portainerContainerEndpoint") ??
                            throw new InvalidOperationException("No portainerContainerEndpoint was found!")));
                embed = new EmbedBuilder
                {
                    Title = "Current Containers:"
                };
                foreach (var container in containerList)
                {
                    embed.AddField(container.Names.First().Replace("/", string.Empty), $"Image: {container.Image}\n" +
                        $"State: {container.State}");
                }
            }
            catch (Exception)
            {
                embed.Title = "Container-Query-Status:";
                embed.AddField("There was an Error while querying Containers!", "Please contact the Admin!");
            }
            ReplyAsync(embed: embed.Build());
            return Task.CompletedTask;
        }
    }
}
