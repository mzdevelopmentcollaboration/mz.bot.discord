﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Discord;
using Discord.Commands;

using Mz.Bot.Discord.Models.Help;
using Mz.Bot.Discord.Models.Configuration;

namespace Mz.Bot.Discord
{
    public partial class HelpModule : ModuleBase<SocketCommandContext>
    {
        private readonly Configuration _config;

        public HelpModule(Configuration config)
        {
            _config = config;
        }

        [Command("help")]
        [Summary("Displays all possible Commands.")]
        public Task GetHelpAsync()
        {
            var embed = new EmbedBuilder
            {
                Title = "Possible Commands:",
                Description = "The following List contains all Commands, which the Bot can handle. " +
                    "All Commands generally are indicated by '!' as the starting Character. " +
                    "For further Questions, please contact the Admin!"
            };
            var commandList = new List<HelpDescription>();
            foreach (var command in _config.commandCollection)
            {
                embed.AddField(command.commandText, command.commandDescription);
            }
            ReplyAsync(embed: embed.Build());
            return Task.CompletedTask;
        }
    }
}
