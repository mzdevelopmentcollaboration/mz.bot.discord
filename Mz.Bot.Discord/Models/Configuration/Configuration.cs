﻿using System.Collections.Generic;

namespace Mz.Bot.Discord.Models.Configuration
{
    public class Configuration
    {
        public List<Command> commandCollection { get; set; }

        public class Command
        {
            public string commandText { get; set; }
            public string commandDescription { get; set; }
        }
    }
}
