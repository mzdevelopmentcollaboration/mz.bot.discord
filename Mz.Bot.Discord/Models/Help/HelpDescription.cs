﻿namespace Mz.Bot.Discord.Models.Help
{
    public class HelpDescription
    {
        public string commandText { get; set; }
        public string commandDescription { get; set; }
    }
}
