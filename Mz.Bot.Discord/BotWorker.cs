using System;
using System.Linq;
using System.Threading;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using Mz.Bot.Discord.Models.Configuration;

namespace Mz.Bot.Discord
{
    public class BotWorker : BackgroundService
    {
        private readonly CommandService _commands;
        private readonly DiscordSocketClient _client;
        private readonly Configuration _config = new();
        private readonly ServiceProvider _serviceProvider;

        public BotWorker(IConfiguration config)
        {
            config.Bind(_config);
            _commands = new CommandService();
            _client = new DiscordSocketClient();
            _serviceProvider = new ServiceCollection().Configure<Configuration>(config)
                .AddTransient(x => x.GetRequiredService<IOptions<Configuration>>().Value)
                .BuildServiceProvider();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _client.MessageReceived += HandleCommandAsync;
                _client.Ready += SetupMetaData;
                await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _serviceProvider);
                await _client.LoginAsync(TokenType.Bot,
                    Environment.GetEnvironmentVariable("discordApplicationSecret"));
                await _client.StartAsync();
                await Task.Delay(-1, stoppingToken);
            }
        }

        private async Task SetupMetaData()
        {
            var mainGuild = _client.Guilds.First(x => x.Id ==
                Convert.ToUInt64(Environment.GetEnvironmentVariable("discordMainGuild")));
            var self = mainGuild.GetUser(_client.CurrentUser.Id);
            await self.ModifyAsync(x => { x.Nickname = "DockerBot"; });
            await _client.SetGameAsync("Managing Docker");
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            if (messageParam is not SocketUserMessage message)
            {
                return;
            }
            var commandIndicatorPosition = 0;
            if (!(message.HasCharPrefix('!', ref commandIndicatorPosition) ||
                message.HasMentionPrefix(_client.CurrentUser, ref commandIndicatorPosition)) ||
                message.Author.IsBot)
            {
                return;
            }
            var context = new SocketCommandContext(_client, message);
            await _commands.ExecuteAsync(context, commandIndicatorPosition, _serviceProvider);
        }
    }
}
